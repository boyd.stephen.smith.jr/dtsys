{-# LANGUAGE CPP, OverloadedStrings #-}

module Lib
	( mainModule
	)
where

-- bytestring
import qualified Data.ByteString as BS

import LLVM.AST
import LLVM.AST.AddrSpace
import LLVM.AST.CallingConvention as CallConv
import LLVM.AST.Constant
import LLVM.AST.FunctionAttribute as FunAttr
import LLVM.AST.Global as Global
import LLVM.AST.InlineAssembly as ASM
import LLVM.AST.Linkage
import LLVM.AST.Visibility

asmSYS_exit0 :: InlineAssembly
asmSYS_exit0 = InlineAssembly
	{ ASM.type' = VoidType
	, hasSideEffects = True
	, alignStack = False -- Default
	, dialect = ATTDialect -- Default
	, assembly = BS.intercalate ("\\0A") [ "movl $$60, %rax", "movl $$0, %rdi", "syscall" ]
	, constraints = "~{rax},~{rdi}"
	}

extExit :: Global
extExit = functionDefaults
	{ returnType = VoidType
	, name = Name "exit"
	, parameters = ([Parameter (IntegerType 32) (UnName 0) []], False)
	}

doNothingSuccessfully :: Global
doNothingSuccessfully = functionDefaults
	{ returnType = VoidType
	, name = Name "_start"
	, Global.functionAttributes = map Right $ [NoReturn, NoUnwind, FunAttr.Cold]
	, basicBlocks = [ block ]
	}
 where
	block = BasicBlock (UnName 0)
		[
		Do (Call Nothing CallConv.Cold [] (Left asmSYS_exit0) [] [] []),
		Do (Call Nothing CallConv.C [] (Right (ConstantOperand (GlobalReference (PointerType (FunctionType VoidType [IntegerType 32] False) (AddrSpace 0)) (Name "exit")))) [(ConstantOperand (Int 32 0), [])] [] [])
		]
		(Do (Unreachable []))

mainModule :: Module
mainModule = Module
	{ moduleName = "main"
	, moduleSourceFileName = __FILE__
	, moduleDataLayout = Nothing
	, moduleTargetTriple = Just "x86_64-linux-gnu"
	, moduleDefinitions = [ GlobalDefinition doNothingSuccessfully, GlobalDefinition extExit ]
	}
